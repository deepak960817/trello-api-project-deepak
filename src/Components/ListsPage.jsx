import React, { Component } from 'react';
import Tabs from './BoardsPageTabs'
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Cards from './Cards';
import * as TrelloAPIData from './TrelloAPIData.js'
import './BoardsPage.css'

class ListsPage extends Component {
    state = { 
        cardNameText: "",
        newCardName: "",
        newListName: "",
        ListNameText: "",
        PageMount: true
    }

    fetchpath = () => {
        let path = window.location.pathname.split("/")[window.location.pathname.split("/").length-1];
        path = path.split('-')[0];
        return path;
    }

    componentDidMount(){
        let path = this.fetchpath();
        if(path!== "")
        {
            TrelloAPIData.getLists(path).then(result => this.setState({ListsData: result, PageMount: false}))
        }
    }

    handleSubmitToCreateList = (e) => {e.preventDefault(); this.setState({newListName: this.state.ListNameText, ListNameText: ""}, ()=>{this.createList()})}

    createList = () => {TrelloAPIData.addLists(this.state.newListName, this.state.ListsData[0].idBoard).then(result => this.setState({ListsData: [result, ...this.state.ListsData]}))}
    
    handleDelete = (id) => {TrelloAPIData.deleteLists(id).then()
        
        let newData = this.state.ListsData.filter((val) => {
            return val.id !== id
        })

        this.setState({ListsData: newData})
    }
    
    render() { 
        
        return ( this.state.PageMount ? <div className='loadingImg' style={{display:'flex', justifyContent:'center'}}><img width={'30%'} height={'30%'} src="http://cdn.onlinewebfonts.com/svg/img_462420.png" alt=""/></div> :
            <div className='bordersPage'>
                <section className='introSection'>
                    <div className='intoSectionLogo'>
                        <img src="https://upload.wikimedia.org/wikipedia/commons/6/6e/D_Magazine_logo.svg" height={'25%'} width={'25%'} alt="" />
                        <h4 className='introSectionHeading'>Deepak Trello Workspace</h4>
                    </div>
                    <Tabs val={"Boards"}/>
                </section>
                <section className='boardsDisplay'>

                <Card style={{ width: '25%', marginLeft: '0.5rem', marginRight: '0.5rem', height:'100%', backgroundColor:'#FFFACD'}}>
                    <Card.Body style={{display:'flex', flexDirection: 'column', alignItems: 'center', flex: '0 0 auto'}}>
                        <Card.Img variant="top" src="https://placeimg.com/200/100/tech" />
                        <Card.Title style={{marginTop: '0.5rem', marginBottom:'0.5rem'}}>Create New List</Card.Title>
                        <Form style={{display: 'flex', flexDirection: 'column'}}>
                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Control type="text" value={this.state.ListNameText} placeholder="Enter List Name" onChange={(e) => {this.setState({ListNameText : e.target.value})}}/>
                        </Form.Group>
                        <Button style={{marginTop: '1rem'}} variant="primary" type="submit" onClick={(e) => {this.handleSubmitToCreateList(e)} }>
                            Submit
                        </Button>
                        </Form>
                    </Card.Body>
                    </Card>

                   {this.state.ListsData.map((val) => {
                    return(
                        <React.Fragment key={val.id}>
                        <Card style={{ width: '25%', marginLeft: '0.5rem', marginRight: '0.5rem', backgroundColor: '#FFFACD'}}>
                            <Card.Body style={{display:'flex', justifyContent:'space-around', flexDirection: 'column', alignItems: 'center', flex: '0 0 auto'}}>
                                <Card.Img variant="top" src="https://picsum.photos/200/100" />
                                <Card.Title style={{marginTop: '0.5rem', marginBottom:'0.5rem'}}>{val.name}</Card.Title>
                                <Button variant='danger' style={{height:'100%', marginLeft: '0.5rem'}} onClick={() => this.handleDelete(val.id)}>Delete List</Button>
                                <Cards id={val.id} />
                            </Card.Body>
                        </Card>
                        </React.Fragment>
                     )})
                    }
                </section>    
            </div>
        );
    }
}
 
export default ListsPage;