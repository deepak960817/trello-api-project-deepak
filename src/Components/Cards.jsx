import React, { Component } from 'react';
import CardModal from './CardModal'
import Modal from 'react-bootstrap/Modal';
import Card from 'react-bootstrap/Card';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import * as TrelloAPIData from './TrelloAPIData.js'

class Cards extends Component {
    state = { 
        cardNameText: "",
        newCardName: "",
        cardsData: [],
        show: false,
        setShow: false,
        checklistShow: false,
        ModalContent: []
    }

    handleClose = () => this.setState({show: false, setShow: false});
    handleShow = (val) => this.setState({show: true, setShow: true}, () => this.changeContent(val));

    componentDidMount(){
        TrelloAPIData.getCards(this.props.id).then(result => this.setState({cardsData: result}))
    }

    changeContent(cardModal){
        this.setState({ModalContent: cardModal})
    }

    handleSubmitToCreateCard = (e) => {
        e.preventDefault();
        this.setState({newCardName: this.state.cardNameText, cardNameText: ""}, () => TrelloAPIData.addCards(this.props.id, this.state.newCardName).then(result => this.setState({cardsData: [...this.state.cardsData,result]})))
    }

    handleDeleteCards = (id) => {TrelloAPIData.deleteCards(id)
        
        let newData = this.state.cardsData.filter((val) => {
            return val.id !== id
        })

        this.setState({cardsData: newData})
    };

    handleClickToShowModal = (e) => {
        e.preventDefault();
        this.setState({checklistShow: true})
    }
    
    render() { 
        return (
                <>
                <Form style={{display: 'flex', flexDirection: 'column', marginTop:'1.5rem'}}>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Control type="text" value={this.state.cardNameText} placeholder="Enter Card Name" onChange={(e) => {this.setState({cardNameText : e.target.value})}}/>
                    </Form.Group>
                    <Button style={{marginTop: '1rem', height: '50%'}} variant="primary" type="submit" onClick={(e) => this.handleSubmitToCreateCard(e)}>Submit</Button>
                </Form>
                {
                this.state.cardsData.map((val) => {
                return (
                <div key={val.id}>
                <Card style={{marginTop: '1rem'}}>
                <Card.Body style={{display:'flex', justifyContent:'space-around', flexDirection: 'column', alignItems: 'center'}}>
                    <Card.Title>{val.name}</Card.Title>
                    <div style={{display: 'flex'}}>
                        <Button variant='primary' style={{height:'100%', marginRight: '0.5rem'}} onClick={() => {this.handleShow(val)}}>View</Button>
                        <Button variant='danger' style={{height:'100%', marginLeft: '0.5rem'}} onClick={() => this.handleDeleteCards(val.id)}>Delete</Button>
                    </div> 
                </Card.Body>
    
                <Modal show={this.state.show} onHide={() => {this.setState({checklistShow: false, show: false})}} size="lg" aria-labelledby="example-modal-sizes-title-lg">
                    <Modal.Header closeButton>
                    <Modal.Title>{this.state.ModalContent.name}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body style={{display:'flex'}}>
                        <section style={{width:'70%'}}>
                            {this.state.checklistShow === true ? <CardModal id={this.state.ModalContent.id}/>: ""}
                        </section>
                        <div style={{display:'flex', width:'30%', maxHeight:'40px', justifyContent:'center'}}>
                            <Button onClick={(e) => this.handleClickToShowModal(e)}>CheckList</Button>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                    <Button variant="secondary" onClick={this.handleClose}>
                        Close
                    </Button>
                    </Modal.Footer>
                </Modal>
                </Card>
                </div>
            )})
        }
        </>
        );
    }
}
 
export default Cards;