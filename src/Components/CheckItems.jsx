import React, { Component } from 'react';
import Form from 'react-bootstrap/Form';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import ProgressBar from 'react-bootstrap/ProgressBar';
import * as TrelloAPIData from './TrelloAPIData.js';

class ChecklistItems extends Component {
    state = { 
        checkItemName: "",
        CheckItemData: [],
        CheckBoxStatus: false,
    }
    
    componentDidMount(){
        TrelloAPIData.getCheckItems(this.props.id).then((result) => {this.setState({CheckItemData: result, checkBoxClick:result.state })});
    }

    handleAddCheckItem(){
        TrelloAPIData.addCheckItems(this.props.id, this.state.checkItemName).then((result) => {this.setState({CheckItemData: [...this.state.CheckItemData, result]})})
    }

    handleUpdateCheckItem(ckeckItemID, status)
    {
        if(status === true)
        {
            TrelloAPIData.updateCheckItem(this.props.cardID, ckeckItemID, 'complete').then(result => {

                const updatedData = this.state.CheckItemData.map((value) => {
                    if(value.id === result.id)
                    {
                        return result;
                    }
                    else
                    {
                        return value;
                    }
                })

                this.setState({CheckItemData: updatedData})
            })
        }
        else
        {
            TrelloAPIData.updateCheckItem(this.props.cardID, ckeckItemID, 'incomplete').then(result => {

                const updatedData = this.state.CheckItemData.map((value) => {
                    if(value.id === result.id)
                    {
                        return result;
                    }
                    else
                    {
                        return value;
                    }
                })

                this.setState({CheckItemData: updatedData})
            })
        }
    }


    handleDeleteCheckList(idChecklist, idCheckItem){
        TrelloAPIData.deleteCheckItem(idChecklist, idCheckItem).then(() => {

            let newData = this.state.CheckItemData.filter((val) => {
                return val.id !== idCheckItem
            })
    
            this.setState({CheckItemData: newData})
        })
    }

    render() { 
        
        return ( 
            <React.Fragment>
                 <ProgressBar style={{margin:'1rem', marginBottom:'2.5rem'}} now={60} label={`60%`} />
                <Form style={{marginBottom:'1rem'}}>
                    <Form.Group className="mb-3" controlId="text">
                    <Form.Control style={{marginTop:'1rem'}} type="text" placeholder="Add a CheckItem" onChange={(e) => {this.setState({checkItemName: e.target.value})}}/>
                    </Form.Group>
                    <div style={{display:'flex', justifyContent:'flex-end', marginTop:'1rem'}}>
                        <Button variant='dark' onClick={() => this.handleAddCheckItem()}>Add new CheckItem</Button>
                    </div>
                    </Form>
               
                {
                    this.state.CheckItemData.map((value) => {
                        let status = value.state === 'complete' ? true : false;
    
                        return (
                            <Card key={value.id} style={{width:'100%', marginTop:'1rem', backgroundColor:'yellow'}}>
                            <Card.Body>
                                <Card.Title>
                                    <Form.Group className="mb-3" controlId="formBasicCheckbox">
                                        <Form.Check type="checkbox" checked={status}  label={<b>{value.name}</b>} onClick={(e) =>{this.handleUpdateCheckItem(value.id, e.target.checked)}}/>
                                    </Form.Group>
                                </Card.Title>
                                <Button variant='danger' onClick={() => this.handleDeleteCheckList(this.props.id, value.id)}>Delete</Button>
                            </Card.Body>
                            </Card>
                        )})
                }
            </React.Fragment>

        );
    }
}
 
export default ChecklistItems;