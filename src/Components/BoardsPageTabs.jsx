import Tab from 'react-bootstrap/Tab';
import Tabs from 'react-bootstrap/Tabs';
import './BoardsPageTabs.css'


function UncontrolledExample(props) {
  return (
    <Tabs
      defaultActiveKey={props.val}
      id="uncontrolled-tab-example"
      className="mb-3">
      <Tab  eventKey="Home" title="Home" disabled>
      </Tab>
      <Tab eventKey="Boards" title="Board" disabled>
      </Tab>
    </Tabs>
  );
}

export default UncontrolledExample;