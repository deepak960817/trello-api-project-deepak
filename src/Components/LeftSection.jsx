import React from 'react';
import Button from 'react-bootstrap/Button';
import './LeftSection.css'
import {Link} from 'react-router-dom'

const LeftSection = () => {
    
    return ( 
        <div className='LeftSection'>
            <div className='sidebarLogo'>
                <img src="https://upload.wikimedia.org/wikipedia/commons/6/6e/D_Magazine_logo.svg" height={'50%'} width={'20%'} alt="" />
                <p className='sidebarHeading'>Deepak Workspace</p>
            </div>
            <div className='sidebarInfo' style={{display:'flex', flexDirection:'column', alignItems:'center'}}>
                <Link to={"/"}>
                    <Button variant='dark' style={{marginTop:'0.5rem', marginBottom:'0.5rem'}}>Home</Button>
                </Link>
            </div>
        </div>
     );
}
 
export default LeftSection;