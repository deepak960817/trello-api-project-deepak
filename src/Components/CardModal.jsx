import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Card from 'react-bootstrap/Card';
import * as TrelloAPIData from './TrelloAPIData.js';
import ChecklistItems from './CheckItems';

class CardModal extends Component {
    state = { 
        itemName: "",
        checklistItemName: "",
        checklistsData: [],
        cardID: ""
    }

    componentDidMount(){
        TrelloAPIData.getChecklists(this.props.id).then((result) => this.setState({checklistsData : result}));
    }

    handleAdd = (id) => {
        this.setState({checklistItemName: this.state.itemName, itemName: ""}, () => this.addChecklist(id));
    }

    addChecklist(id){
        TrelloAPIData.addChecklist(id, this.state.checklistItemName).then((result) => this.setState({checklistsData: [...this.state.checklistsData, result]}))
    }

    handleDelete(id){
        TrelloAPIData.deleteChecklist(id);

        let newData = this.state.checklistsData.filter((val) => {
            return val.id !== id
        })

        this.setState({checklistsData: newData})
    }

    render() { 
        
        return (
            <section>
            <div style={{display:'flex', justifyContent:'space-between', marginBottom:'1rem', alignItems:'center'}}>
                <h5>CheckList</h5>
            </div>
            
            {
            this.state.checklistsData.map((val) => {
            return(
                <Card key={val.id} style={{width:'100%', marginTop:'1rem', backgroundColor:'#E8E8E8'}}>
                <Card.Body>
                    <Card.Title>
                            <h4><b>{val.name}</b></h4>
                    </Card.Title>
                    <ChecklistItems cardID={this.props.id} id={val.id} />
                    <div style={{display:'flex', justifyContent:'flex-end'}}>
                    <Button variant='danger' style={{marginTop: '1rem'}} onClick={() => this.handleDelete(val.id)}>Delete Checklist</Button>
                    </div>
                </Card.Body>
                </Card>
                )})
            }

            <Form style={{marginBottom:'1rem'}}>
                <Form.Group className="mb-3" controlId="text">
                <Form.Control style={{marginTop:'1rem'}} type="text" value={this.state.itemName} placeholder="Add an Item" onChange={(e) => {this.setState({itemName: e.target.value})}}/>
                </Form.Group>
                <div style={{display:'flex', justifyContent:'flex-start'}}>
                    <Button style={{marginTop:'1rem'}} onClick={() => {this.handleAdd(this.props.id)}}>Add</Button>
                </div>
            </Form>
            </section>
        );
    }
}
 
export default CardModal;