import axios from 'axios';

axios.defaults.params = {
    key : "ce26ffdbf3ab54db085570e359fe543b",
    token: "cc85dd788c5d9ce84a93da1099e4578fbb3333bdaaf3dcaafb57917769251e8c"
}


export function getBoards() {
    return axios.get('https://api.trello.com/1/members/me/boards')
    .then((result) => result.data)
}

export function addBoards(name) {
    return axios.post('https://api.trello.com/1/boards', null, {
        params : {
            name
        }
    }).then((result) => result.data) 
}

export function deleteBoards(id) {
    return axios.delete(`https://api.trello.com/1/boards/${id}`)
    .then((result) => result.data) 
}

export function getLists(id) {
    return axios.get(`https://api.trello.com/1/boards/${id}/lists`)
    .then((result) => result.data) 
}

export function addLists(name, idBoard) {
    return axios.post('https://api.trello.com/1/lists', null, {
        params : {
            name,
            idBoard
        }
    }).then((result) => result.data) 
}

export function deleteLists(id) {
    return axios.put(`https://api.trello.com/1/lists/${id}/closed`, null, {
        params : {
            value : true
        }
    })
    .then((result) => result.data) 
}

export function getCards(id) {
    return axios.get(`https://api.trello.com/1/lists/${id}/cards`)
    .then((result) => result.data) 
}

export function addCards(idList, name) {
    return axios.post('https://api.trello.com/1/cards', null, {
        params : {
            idList,
            name
        }
    }).then((result) => result.data) 
}

export function deleteCards(id) {
    return axios.delete(`https://api.trello.com/1/cards/${id}`, null, {
    }).then(() => {}) 
}

export function addChecklist(idCard, name) {
    return axios.post(`https://api.trello.com/1/checklists`, null, {
        params : {
            idCard,
            name
        }
    })
    .then((result) => result.data)
}

export function getChecklists(id) {
    return axios.get(`https://api.trello.com/1/cards/${id}/checklists`)
    .then((result) => result.data) 
}

export function deleteChecklist(id) {
    return axios.delete(`https://api.trello.com/1/checklists/${id}`, null, {
    }).then((result) => result.data) 
}

export function getCheckItems(id){
    return axios.get(`https://api.trello.com/1/checklists/${id}/checkItems`)
    .then((result) => result.data)
}

export function addCheckItems(id, name) {
    
    return axios.post(`https://api.trello.com/1/checklists/${id}/checkItems`, null, {
        params : {
            name
        }
    }).then((result) => result.data)
}

export function updateCheckItem(id, idCheckItem, state){

    return axios.put(`https://api.trello.com/1/cards/${id}/checkItem/${idCheckItem}`, null, {
        params : {
            state
        }
    })
    .then((result) => result.data)
}

export function deleteCheckItem(id, idCheckItem) {
    return axios.delete(`https://api.trello.com/1/checklists/${id}/checkItems/${idCheckItem}`)
    .then((result) => result.data) 
}

// /1/cards/{id}/checkItem/{idCheckItem}
