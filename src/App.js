import React, { Component } from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

import Header from './Components/Header';
import LeftSection from './Components/LeftSection';
import BoardsPage from './Components/BoardsPage';
import ListsPage from './Components/ListsPage';


class App extends Component {

  state ={
    PageMount: false,
    path: ""
  }

  fetchBoardID = () => { 
    return window.location.pathname.split("/")[window.location.pathname.split("/").length-1];
  }

  componentDidMount(){
    this.setState({PageMount:true})
  }

  pathToListPage = (listPath) => {
    this.setState({path: listPath})
  }

  render() { 
    
    return ( this.state.PageMount ?
      <React.Fragment>
        <section>
          <Header />
          <section style={{display:'flex'}}>
            <Router>
              <Routes>
                <Route exact path={"/"} element={
                  <>
                  <LeftSection />
                   <BoardsPage pathToListPage={this.pathToListPage}/>
                   </>
                  }/>
                  <Route path={`/:${this.state.path}`} element={
                  <>
                  <LeftSection />
                    <ListsPage />
                  </>
                }/>
              </Routes>
            </Router>
            
          </section>
        </section>
        
      </React.Fragment>
      : <div className='loadingImg' style={{display:'flex', justifyContent:'center'}}><img width={'30%'} height={'30%'} src="http://cdn.onlinewebfonts.com/svg/img_462420.png" alt=""/></div>
    );
  }
}
 
export default App;

